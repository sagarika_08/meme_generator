const form = document.getElementById('form1')
let imgColumn;
form.addEventListener('submit', addImage)
//ADD IMAGE
function addImage(e){
    e.preventDefault()
    const imgUrl = document.getElementById('imgUrl').value
    if (imgUrl == "") {
        alert('Enter URL for your image')
    }
    else {
        const textOnTop = document.getElementById('textOnTop').value;
        const textOnBottom = document.getElementById('textOnBottom').value
        const displayOutput = document.getElementById('displayOutput')



        fetch(imgUrl)
            .then(response => {
                console.log(response)
                if (!response.ok) {
                    alert('Invalid URL');
                    throw err 
                }
                else
                return response.blob();
            })
            .then(blob => {
                console.log(blob)
                const newImg = URL.createObjectURL(blob)
                return(newImg)
            })
            .then(newImg=>{
                console.log('new',newImg)
                //create div for img
                const imgDiv = document.createElement('div')
                imgDiv.className = "column";
                imgDiv.id = "imgColumn"
                 //create img div
                const img = document.createElement('img')
                img.src = newImg
                img.className="img"
                img.id="img"
                 //create button
                const deleteButton = document.createElement('button');
                deleteButton.innerText="X"
                deleteButton.className="delete"
                deleteButton.id = 'delete'
                imgDiv.appendChild(img)
                imgDiv.appendChild(deleteButton)
                displayOutput.appendChild(imgDiv)
        
                if(textOnTop != ''){
                    const newTopDiv = document.createElement('div')
                    newTopDiv.innerText =textOnTop
                    newTopDiv.className="topDiv"
                    imgDiv.appendChild(newTopDiv)
                }
                if(textOnBottom!=''){
                    const newBottomDiv = document.createElement('div')
                    newBottomDiv.innerText =textOnBottom
                    newBottomDiv.className="BottomDiv"
                    imgDiv.appendChild(newBottomDiv)
                }
                
            })
            .catch(err=>{
                console.error('error print',err)
            })

        form.reset()
        // console.log('imgUrl', imgUrl)
        // console.log('textOnTop', textOnTop)
        // console.log('textOnBottom', textOnBottom)
    }
}

//REMOVE IMAGE
displayOutput=document.getElementById('displayOutput')
displayOutput.addEventListener('click',removeImage)
function removeImage(e){
    e.preventDefault()
    if(e.target.classList.contains('delete')){
        var removeParentColumn=e.target.parentElement;
        displayOutput.removeChild(removeParentColumn)
    }
}

//CHANGE TEXTBOX COLOR
// document.querySelectorAll('input[type=text]').forEach(input =>{
//     input.addEventListener('blur',(e)=>{
//         e.preventDefault()
//         console.log('inside')
//         e.target.style.backgroundColor="#FFF7CC"
       
//     })
// })
